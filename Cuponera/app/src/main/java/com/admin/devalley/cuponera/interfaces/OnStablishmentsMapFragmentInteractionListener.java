package com.admin.devalley.cuponera.interfaces;

import com.admin.devalley.cuponera.models.Model_Stablishment;

/**
 * Created by user on 26/03/2015.
 */
public interface OnStablishmentsMapFragmentInteractionListener {
    public void onMarkerSelected(Model_Stablishment modelStablishment);
}
