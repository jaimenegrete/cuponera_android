package com.admin.devalley.cuponera.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.admin.devalley.cuponera.R;
import com.admin.devalley.cuponera.camview.ScannerView;
import com.admin.devalley.cuponera.config.Config;
import com.admin.devalley.cuponera.helpers.JSONToModelsConverter;
import com.admin.devalley.cuponera.models.Model_Stablishment;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ScannerFragment extends Fragment {

    public static ScannerFragment newInstance() {
        return new ScannerFragment();
    }

    public ScannerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        stablishments = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getActionBar().setTitle("Lector de Cupones");
        mainContext = getActivity();
        mainScannerView = inflater.inflate(R.layout.fragment_lector, container, false);
        textViewCode = (TextView) mainScannerView.findViewById(R.id.codigoTV);
        textViewCode.setText("");
        scanner = (ScannerView) mainScannerView.findViewById(R.id.scanner);
        scanner.setScannerViewEventListener(new ScannerView.ScannerViewEventListener()
        {
            public boolean onCodeScanned(final String data)
            {
                scanner.stopScanner();
                couponScannedId = Integer.parseInt(data);
                getStablishmentsOfAdminFromWebService();
                scannerButton.setEnabled(true);
                return true;
            }
        });
        scanner.startScanner();


        scannerButton = (Button) mainScannerView.findViewById(R.id.button);
        scannerButton.setEnabled(false);
        scannerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scanner.startScanner();
                scannerButton.setEnabled(false);
            }
        });

        return mainScannerView;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        scanner.stopScanner();
    }

    private void showConfirmDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage("¿Desea canjear el cupón?")
                .setTitle("Cupón válido");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
                redeemCoupon();
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void getStablishmentsOfAdminFromWebService(){
        AsyncHttpClient asyncClient = new AsyncHttpClient();
        RequestParams requestParams = new RequestParams();

        String url = STABLISHMENTS_URL_API + Config.getInstance().getUser().adminId;
        progressDialog = ProgressDialog.show(getActivity(), "Conectando", "Validando cupón...", true);
        asyncClient.get(url, requestParams, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode,
                                  org.apache.http.Header[] headers,
                                  java.lang.Throwable throwable,
                                  org.json.JSONObject errorResponse){
                Toast.makeText(getActivity(), "Ocurrió un error en el servidor", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray json) {

                try {
                    stablishments.clear();
                    for (int i = 0; i < json.length(); i++){
                        JSONObject stablishmentJSON = json.getJSONObject(i);
                        stablishments.add(JSONToModelsConverter.convertJSONToStablishment(stablishmentJSON));
                    }
                    getStablishmentOfCouponFromWebService(couponScannedId);

                } catch (JSONException JSONException) {
                    JSONException.printStackTrace();
                }
            }

            @Override
            public void onFinish() {
            }
        });
    }

    private void getStablishmentOfCouponFromWebService(int couponId){
        AsyncHttpClient asyncClient = new AsyncHttpClient();
        RequestParams requestParams = new RequestParams();

        String url = STABLISHMENT_BY_COUPON_URL_API + couponId;

        asyncClient.get(url, requestParams, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode,
                                  org.apache.http.Header[] headers,
                                  java.lang.Throwable throwable,
                                  org.json.JSONObject errorResponse){
                Toast.makeText(getActivity(), "Ocurrió un error en el servidor", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray json) {

                try {
                    checkIfAdminIsValid(JSONToModelsConverter.convertJSONToStablishment(json.getJSONObject(0)));
                    progressDialog.dismiss();
                } catch (JSONException exception) {
                    exception.printStackTrace();
                }
            }

            @Override
            public void onFinish() {
            }
        });
    }

    private void checkIfAdminIsValid(Model_Stablishment stablishment){
        for(Model_Stablishment validStablishment: stablishments){
            if(validStablishment.id == stablishment.id){
                showConfirmDialog();
                return;
            }
        }
        Toast.makeText(getActivity(), "Cupón inválido para este establecimiento", Toast.LENGTH_LONG).show();
    }

    private void redeemCoupon(){
        AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
        progressDialog = ProgressDialog.show(getActivity(), "Canjeando", "Espere por favor...", true);

        String url = INCREASED_USED_COUPON_URL_API + couponScannedId;
        asyncHttpClient.get(url, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode,
                                  org.apache.http.Header[] headers,
                                  java.lang.Throwable throwable,
                                  org.json.JSONObject errorResponse) {
                Toast.makeText(getActivity(), "Ocurrió un error en el servidor", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject json) {
                try {
                    if(json.has("response")){
                        if (json.getBoolean("response")){
                            Toast.makeText(getActivity(), "Cupón canjeado", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getActivity(), "No se pudo canjear el cupón", Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        Toast.makeText(getActivity(), "No se pudo canjear el cupón", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }

            @Override
            public void onFinish() {
                progressDialog.dismiss();
            }
        });
    }

    private ActionBar getActionBar() {
        return ((ActionBarActivity) getActivity()).getSupportActionBar();
    }

    private View mainScannerView;
    private ScannerView scanner;
    private Button scannerButton;
    private TextView textViewCode;
    private Context mainContext;
    private int couponScannedId;
    private ArrayList<Model_Stablishment> stablishments;
    private ProgressDialog progressDialog;
    private final String STABLISHMENTS_URL_API = Config.MAIN_URL_API + "stablishments/";
    private final String STABLISHMENT_BY_COUPON_URL_API = Config.MAIN_URL_API + "stablishment/byCoupon/";
    private final String INCREASED_USED_COUPON_URL_API = Config.MAIN_URL_API + "coupon/increaseUsedValue/";
}
