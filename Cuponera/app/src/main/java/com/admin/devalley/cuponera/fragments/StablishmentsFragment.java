package com.admin.devalley.cuponera.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.admin.devalley.cuponera.R;
import com.admin.devalley.cuponera.config.Config;
import com.admin.devalley.cuponera.helpers.JSONToModelsConverter;
import com.admin.devalley.cuponera.interfaces.OnStablishmentsFragmentInteractionListener;
import com.admin.devalley.cuponera.listAdapters.StablishmentsAdapter;
import com.admin.devalley.cuponera.models.Model_Stablishment;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class StablishmentsFragment extends Fragment implements AbsListView.OnItemClickListener, AdapterView.OnItemSelectedListener{

    public static StablishmentsFragment newInstance() {
        return new StablishmentsFragment();
    }

    public StablishmentsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        getActionBar().setTitle("Establecimientos");

        mainStablishmentsView = inflater.inflate(R.layout.fragment_establecimientos, container, false);
        stablishmentsListView = (AbsListView) mainStablishmentsView.findViewById(android.R.id.list);
        stablishmentsListView.setOnItemClickListener(this);
        Spinner stablishmentTypeSpinner = (Spinner) mainStablishmentsView.findViewById(R.id.stablishmentTypesSpinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.stablishmentsTypesArray, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        stablishmentTypeSpinner.setAdapter(adapter);
        stablishmentTypeSpinner.setOnItemSelectedListener(this);
        stablishments = new ArrayList<Model_Stablishment>();
        getDataFromWebService();

        return mainStablishmentsView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (null != stablishmentsListener) {
            // Notify the active callbacks interface (the activity, if the
            // fragment is attached to one) that an item has been selected.
            stablishmentsListener.onStablishmentSelected(stablishments.get(position));
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            stablishmentsListener = (OnStablishmentsFragmentInteractionListener) activity;
        } catch (ClassCastException exception) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        stablishmentsListener = null;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String stablishmentType = parent.getItemAtPosition(position).toString();
        filterStablishmentsByType(stablishmentType);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void getDataFromWebService(){
        AsyncHttpClient asyncClient = new AsyncHttpClient();
        RequestParams requestParams = new RequestParams();

        String url = STABLISHMENTS_URL_API + Config.getInstance().getUser().adminId;
        asyncClient.get(url, requestParams, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode,
                                  org.apache.http.Header[] headers,
                                  java.lang.Throwable throwable,
                                  org.json.JSONObject errorResponse){
                Toast.makeText(getActivity(), "Ocurrió un error en el servidor", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray json) {

                try {
                    stablishments.clear();
                    for (int i = 0; i < json.length(); i++){
                        JSONObject stablishmentJSON = json.getJSONObject(i);
                        stablishments.add(JSONToModelsConverter.convertJSONToStablishment(stablishmentJSON));
                    }
                    refreshView(stablishments);

                } catch (JSONException JSONException) {
                    JSONException.printStackTrace();
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

    private void refreshView(ArrayList<Model_Stablishment> stablishmentsArray){
        stablishmentsAdapter = new StablishmentsAdapter(getActivity(),R.layout.stablishment_list_layout, stablishmentsArray);
        ((AdapterView<ListAdapter>) stablishmentsListView).setAdapter(stablishmentsAdapter);
    }

    private ActionBar getActionBar() {
        return ((ActionBarActivity) getActivity()).getSupportActionBar();
    }

    private void filterStablishmentsByType(String stablishmentType){
        ArrayList<Model_Stablishment> filteredStablishmentsArray= new ArrayList<>();
        for(Model_Stablishment stablishment: stablishments){
            if(stablishment.stablishmentType.equals(stablishmentType)){
                filteredStablishmentsArray.add(stablishment);
            }
        }
        refreshView(filteredStablishmentsArray);
    }

    private OnStablishmentsFragmentInteractionListener stablishmentsListener;
    private View mainStablishmentsView;
    private StablishmentsAdapter stablishmentsAdapter;
    private ArrayList<Model_Stablishment> stablishments;
    private AbsListView stablishmentsListView;
    private final String STABLISHMENTS_URL_API = Config.MAIN_URL_API + "stablishments/";
}
