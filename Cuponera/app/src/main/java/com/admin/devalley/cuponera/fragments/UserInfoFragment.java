package com.admin.devalley.cuponera.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.admin.devalley.cuponera.R;
import com.admin.devalley.cuponera.config.Config;
import com.admin.devalley.cuponera.interfaces.OnUserInfoFragmentInteractionListener;
import com.admin.devalley.cuponera.models.Model_User;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by cecia on 03/04/2015.
 */
public class UserInfoFragment extends Fragment {

    public Model_User modelUser;

    public static UserInfoFragment newInstance() {
        UserInfoFragment fragment = new UserInfoFragment();
        fragment.modelUser = Config.getInstance().getUser();
        return fragment;
    }

    public UserInfoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getActionBar().setTitle("Mi Perfil");
        mainUserInfoView = inflater.inflate(R.layout.fragment_user_info, container, false);
        usernameTxt = (TextView)mainUserInfoView.findViewById(R.id.usernameTxt);
        usernameTxt.setText(modelUser.username);

        nameTxt = (TextView)mainUserInfoView.findViewById(R.id.nameTxt);
        emailTxt = (TextView)mainUserInfoView.findViewById(R.id.emailTxt);

        editInfoBtn = (Button)mainUserInfoView.findViewById(R.id.editInfoBtn);

        if (modelUser.userType == Model_User.UserType.ADMIN.id){
            nameTxt.setVisibility(View.INVISIBLE);
            mainUserInfoView.findViewById(R.id.nameLabel).setVisibility(View.INVISIBLE);
            emailTxt.setVisibility(View.INVISIBLE);
            mainUserInfoView.findViewById(R.id.emailLabel).setVisibility(View.INVISIBLE);
            editInfoBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    userInfoListener.onEditUserInfoSelected("admin","admin","admin@mail.com");
                }
            });
        }else{
            editInfoBtn.setVisibility(View.INVISIBLE);
            editInfoBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    userInfoListener.onEditUserInfoSelected(name,lastName,email);
                }
            });
            getInfoFromWebService();
        }



        return mainUserInfoView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            userInfoListener = (OnUserInfoFragmentInteractionListener) activity;
        } catch (ClassCastException exception) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        userInfoListener = null;
    }

    private void getInfoFromWebService(){
        AsyncHttpClient asyncClient = new AsyncHttpClient();

        String url = CLIENT_URL_API + modelUser.id;
        asyncClient.get(url, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode,
                                  org.apache.http.Header[] headers,
                                  java.lang.Throwable throwable,
                                  org.json.JSONObject errorResponse){
                Toast.makeText(getActivity(), "Ocurrió un error en el servidor", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject json) {
                try {
                    name = json.getString("name");
                    lastName = json.getString("last_name");
                    email = json.getString("email");

                    nameTxt.setText(name + " " + lastName);
                    emailTxt.setText(email);

                    editInfoBtn.setVisibility(View.VISIBLE);
                } catch (JSONException exception) {
                    exception.printStackTrace();
                    Toast.makeText(getActivity(),"Cliente inválido :(",Toast.LENGTH_SHORT).show();
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }

            @Override
            public void onFinish() {
            }
        });
    }
    private ActionBar getActionBar() {
        return ((ActionBarActivity) getActivity()).getSupportActionBar();
    }

    private String name, lastName, email;
    private TextView nameTxt, emailTxt, usernameTxt;
    private OnUserInfoFragmentInteractionListener userInfoListener;
    private Button editInfoBtn;
    private View mainUserInfoView;
    private final String CLIENT_URL_API = Config.MAIN_URL_API +"clientByUserId/";

}
