package com.admin.devalley.cuponera.models;

import java.util.Date;

/**
 * Created by user on 26/03/2015.
 */
public class Model_Coupon {
    public int id;
    public int stablishment_id;
    public String stablishmentType;
    public String name;
    public Date startDate;
    public Date endDate;
    public String image;
    public String description;
    public boolean status;
    public String qrImage;
    public int downloaded;
    public int acquired;
    public int used;

    public Model_Coupon(int id, String name, Date startDate, Date endDate, String image, String description, boolean status, String qrImage, int downloaded, int acquired, int used) {
        this.id = id;
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.image = image;
        this.description = description;
        this.status = status;
        this.qrImage = qrImage;
        this.downloaded = downloaded;
        this.acquired = acquired;
        this.used = used;
        this.stablishmentType = "OTRO";
    }

}
