package com.admin.devalley.cuponera.interfaces;

/**
 * Created by user on 26/03/2015.
 */
public interface OnContactDataRegistrationFragmentListener {
    public void goToNextStep(int position, String nombre, String apellido, String mail);
}
