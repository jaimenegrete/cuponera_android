package com.admin.devalley.cuponera.activities;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.admin.devalley.cuponera.R;
import com.admin.devalley.cuponera.fragments.ClientNavigationDrawerFragment;
import com.admin.devalley.cuponera.fragments.CouponFragment;
import com.admin.devalley.cuponera.fragments.CouponsFragment;
import com.admin.devalley.cuponera.fragments.MyCouponFragment;
import com.admin.devalley.cuponera.fragments.MyCouponsFragment;
import com.admin.devalley.cuponera.fragments.StablishmentFragment;
import com.admin.devalley.cuponera.fragments.StablishmentInfoFragment;
import com.admin.devalley.cuponera.fragments.StablishmentsMapFragment;
import com.admin.devalley.cuponera.fragments.UserEditInfoFragment;
import com.admin.devalley.cuponera.fragments.UserInfoFragment;
import com.admin.devalley.cuponera.interfaces.NavigationDrawerCallbacks;
import com.admin.devalley.cuponera.interfaces.OnCouponFragmentInteractionListener;
import com.admin.devalley.cuponera.interfaces.OnCouponsFragmentInteractionListener;
import com.admin.devalley.cuponera.interfaces.OnStablishmentFragmentInteractionListener;
import com.admin.devalley.cuponera.interfaces.OnStablishmentsFragmentInteractionListener;
import com.admin.devalley.cuponera.interfaces.OnStablishmentsMapFragmentInteractionListener;
import com.admin.devalley.cuponera.interfaces.OnUserInfoFragmentInteractionListener;
import com.admin.devalley.cuponera.models.Model_Coupon;
import com.admin.devalley.cuponera.models.Model_Stablishment;


public class ClientMainActivity extends ActionBarActivity
        implements NavigationDrawerCallbacks,
        OnUserInfoFragmentInteractionListener,
        OnCouponsFragmentInteractionListener,
        OnCouponFragmentInteractionListener,
        OnStablishmentsFragmentInteractionListener,
        OnStablishmentsMapFragmentInteractionListener,
        OnStablishmentFragmentInteractionListener {

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        /*

        for(int i = 0; i < fragmentManager.getBackStackEntryCount(); ++i) {
            fragmentManager.popBackStack();
        }
        */
        switch (position){
            case EDIT_PROFILE_POSITION:
                fragmentManager.beginTransaction()
                        .replace(R.id.container, UserInfoFragment.newInstance()).commit();
                break;
            case ACQUIRED_COUPONS_POSITION:
                fragmentManager.beginTransaction()
                        .replace(R.id.container, CouponsFragment.newInstance()).commit();
                break;
            case CLOSE_TO_ME_POSITION:
                fragmentManager.beginTransaction()
                        .replace(R.id.container, StablishmentsMapFragment.newInstance()).commit();
                break;
            case MY_COUPONS_POSITION:
                fragmentManager.beginTransaction()
                        .replace(R.id.container, MyCouponsFragment.newInstance()).commit();
                break;
        }

    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                activityTitle = getString(R.string.title_section1);
                break;
            case 2:
                activityTitle = getString(R.string.title_section2);
                break;
            case 3:
                activityTitle = getString(R.string.title_section3);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(activityTitle);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!clientNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            //restoreActionBar();
            return false;
        }
        //return super.onCreateOptionsMenu(menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStablishmentSelected(Model_Stablishment modelStablishment) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, StablishmentFragment.newInstance(modelStablishment))
                .addToBackStack("elementos")
                .commit();
    }

    @Override
    public void onMarkerSelected(Model_Stablishment modelStablishment) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, StablishmentFragment.newInstance(modelStablishment))
                .addToBackStack("elementos")
                .commit();
    }

    @Override
    public void onCouponSelected(Model_Coupon coupon, Model_Stablishment stablishment) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, CouponFragment.newInstance(coupon, stablishment))
                .addToBackStack("elementos")
                .commit();
    }

    @Override
    public void onCouponSelected(Model_Coupon coupon) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, CouponFragment.newInstance(coupon, null))
                .addToBackStack("elementos")
                .commit();
    }

    @Override
    public void onMyCouponSelected(Model_Coupon coupon) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, MyCouponFragment.newInstance(coupon, null))
                .addToBackStack("elementos")
                .commit();
    }

    @Override
    public void onShowStablishmentSelected(Model_Stablishment modelStablishment) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, StablishmentInfoFragment.newInstance(modelStablishment))
                .addToBackStack("elementos")
                .commit();
    }

    @Override
    public void onShowStablishmentSelectedWithId(int stablismentId) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, StablishmentInfoFragment.newInstanceWithId(stablismentId))
                .addToBackStack("elementos")
                .commit();
    }

    @Override
    public void onEditUserInfoSelected(String name, String lastName, String email) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, UserEditInfoFragment.newInstance(name,lastName,email))
                .addToBackStack("elementos")
                .commit();
    }

    @Override
    public void onUserEditInfoSaved() {
        getSupportFragmentManager().popBackStack();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_main);
        final Context context = getBaseContext();

        clientNavigationDrawerFragment = (ClientNavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        activityTitle = getTitle();

        clientNavigationDrawerFragment.initializeClientNavigationFragmentFromActivity(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
        mainContext = this;
    }

    private ClientNavigationDrawerFragment clientNavigationDrawerFragment;
    private CharSequence activityTitle;
    private Context mainContext;
    private final int EDIT_PROFILE_POSITION = 0;
    private final int ACQUIRED_COUPONS_POSITION = 1;
    private final int CLOSE_TO_ME_POSITION = 2;
    private final int MY_COUPONS_POSITION = 3;
}
