package com.admin.devalley.cuponera.models;

import java.util.ArrayList;

public class Model_Stablishment {

    public int id;
    public String name;
    public String address;
    public String description;
    public String image;
    public String stablishmentType;
    public double latitude;
    public double longitude;
    public String phone;
    public ArrayList<Model_Coupon> coupons;

    public Model_Stablishment(int id, String name, String address, String description, String image, String stablishmentType, double latitude, double longitude, String phone) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.description = description;
        this.image = image;
        this.stablishmentType = stablishmentType;
        this.latitude = latitude;
        this.longitude = longitude;
        this.phone = phone;
    }
}
