package com.admin.devalley.cuponera.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.admin.devalley.cuponera.R;
import com.admin.devalley.cuponera.config.Config;
import com.admin.devalley.cuponera.interfaces.OnUserInfoFragmentInteractionListener;
import com.admin.devalley.cuponera.models.Model_User;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserEditInfoFragment extends Fragment {

    public String name, lastName, email;

    public static UserEditInfoFragment newInstance(String name,String lastName, String email) {
        UserEditInfoFragment fragment = new UserEditInfoFragment();
        fragment.name = name;
        fragment.lastName = lastName;
        fragment.email = email;
        return fragment;
    }

    public UserEditInfoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        modelUser = Config.getInstance().getUser();
        emailVerificationPattern = Pattern.compile(EMAIL_PATTERN);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getActionBar().setTitle("Editando perfil");
        mainUserInfoView = inflater.inflate(R.layout.fragment_edit_user_info, container, false);
        nameField = (EditText)mainUserInfoView.findViewById(R.id.edit_nombreEdit);
        nameField.setText(name);
        lastNameField = (EditText)mainUserInfoView.findViewById(R.id.edit_apellidoEdit);
        lastNameField.setText(lastName);
        emailField = (EditText)mainUserInfoView.findViewById(R.id.edit_emailEdit);
        emailField.setText(email);

        usernameField = (EditText)mainUserInfoView.findViewById(R.id.edit_usuarioEdit);
        usernameField.setText(Config.getInstance().getUser().username);
        passwordField = (EditText)mainUserInfoView.findViewById(R.id.edit_passEdit);
        passwordConfirmField = (EditText)mainUserInfoView.findViewById(R.id.edit_passConfirmEdit);

        saveUserInfoButton = (Button)mainUserInfoView.findViewById(R.id.edit_guardarBtn);

        if (modelUser.userType == Model_User.UserType.ADMIN.id){
            nameField.setVisibility(View.INVISIBLE);
            lastNameField.setVisibility(View.INVISIBLE);
            emailField.setVisibility(View.INVISIBLE);
        }

        saveUserInfoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkFields();
            }
        });
        return mainUserInfoView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            userEditListener = (OnUserInfoFragmentInteractionListener) activity;
        } catch (ClassCastException exception) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        userEditListener = null;
    }

    private void checkFields(){
        if (!isEmailValid(emailField)){
            Toast.makeText(getActivity(), "Proporcione un email válido", Toast.LENGTH_LONG).show();
            return;
        }
        if(isEmpty(usernameField)){
            Toast.makeText(getActivity(), "Llene todos los campos", Toast.LENGTH_LONG).show();
        }else{
            if (isAccountDataValid()){
                if (passwordField.getText().toString().equals(passwordConfirmField.getText().toString()))
                    validateUserName();
                else
                    Toast.makeText(getActivity(), "Las contraseñas no coinciden", Toast.LENGTH_LONG).show();
            }else {
                Toast.makeText(getActivity(), "Llene todos los campos", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void validateUserName(){
        AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
        progressDialog = ProgressDialog.show(mainUserInfoView.getContext(), "Actualizando", "Validando nombre de usuario...", true);
        String url = VALIDATE_USERNAME_URL_API + usernameField.getText().toString();
        asyncHttpClient.get(url, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode,
                                  org.apache.http.Header[] headers,
                                  java.lang.Throwable throwable,
                                  org.json.JSONObject errorResponse) {
                Toast.makeText(mainUserInfoView.getContext(), "Ocurrió un error en el servidor", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject json) {
                try {
                    if(json.has("error")){
                        validateClientMail();
                    }else{
                        if (json.getInt("id") == modelUser.id){

                            if (modelUser.userType == Model_User.UserType.ADMIN.id){
                                updateUserInfo();
                            }else{
                                validateClientMail();
                            }

                        }else{
                            Toast.makeText(mainUserInfoView.getContext(), "El nombre de usuario ya existe", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }
                    }
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }

            @Override
            public void onFinish() {
            }
        });
    }

    private void validateClientMail(){
        AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
        progressDialog.setMessage("Validando email...");
        String url = VALIDATE_CLIENT_MAIL_URL_API + email;
        asyncHttpClient.get(url, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode,
                                  org.apache.http.Header[] headers,
                                  java.lang.Throwable throwable,
                                  org.json.JSONObject errorResponse) {
                Toast.makeText(mainUserInfoView.getContext(), "Ocurrió un error en el servidor", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject json) {
                try {
                    if(json.has("success")){
                        updateUserInfo();
                    }else{
                        if (json.getInt("user_id") == modelUser.id){
                            updateUserInfo();
                        }else{
                            Toast.makeText(mainUserInfoView.getContext(), "El email ya ha sido utilizado", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }
                    }
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }

            @Override
            public void onFinish() {
            }
        });
    }

    private void updateUserInfo(){

        AsyncHttpClient asyncHttpClient = new AsyncHttpClient();

        JSONObject jsonUser = new JSONObject();
        try {
            jsonUser.put("username", usernameField.getText().toString());
            jsonUser.put("password", passwordField.getText().toString());
        } catch (JSONException JSONException) {
            JSONException.printStackTrace();
        }

        StringEntity entity = null;
        try {
            entity = new StringEntity(jsonUser.toString());
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
        } catch (UnsupportedEncodingException exception) {
            exception.printStackTrace();
        }
        progressDialog.setMessage("Enviando información...");
        String url = EDIT_USER_URL_API + modelUser.id;

        asyncHttpClient.put(getActivity(),url, entity, "application/json", new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode,
                                  org.apache.http.Header[] headers,
                                  java.lang.Throwable throwable,
                                  org.json.JSONObject errorResponse){
                Toast.makeText(mainUserInfoView.getContext(), "Ocurrió un error en el servidor", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject json) {

                try {
                    Config.getInstance().getUser().username = usernameField.getText().toString();

                    if (modelUser.userType == Model_User.UserType.ADMIN.id){
                        backToPreviousView();
                    }else{
                        updateClientInfo();
                    }

                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }

            @Override
            public void onFinish() {
            }
        });
    }

    private void updateClientInfo(){

        AsyncHttpClient asyncHttpClient = new AsyncHttpClient();

        JSONObject jsonContactData = new JSONObject();
        try {
            jsonContactData.put("name", nameField.getText().toString());
            jsonContactData.put("lastName", lastNameField.getText().toString());
            jsonContactData.put("email", emailField.getText().toString());
            jsonContactData.put("status", ACTIVE_STATUS);
        } catch (JSONException JSONException) {
            JSONException.printStackTrace();
        }

        StringEntity entity = null;
        try {
            entity = new StringEntity(jsonContactData.toString());
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
        } catch (UnsupportedEncodingException exception) {
            exception.printStackTrace();
        }
        progressDialog.setMessage("Enviando información...");
        String url = EDIT_CLIENT_URL_API + modelUser.clientId;

        asyncHttpClient.put(getActivity(),url, entity, "application/json", new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode,
                                  org.apache.http.Header[] headers,
                                  java.lang.Throwable throwable,
                                  org.json.JSONObject errorResponse){
                Toast.makeText(mainUserInfoView.getContext(), "Ocurrió un error en el servidor", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject json) {
                backToPreviousView();
            }

            @Override
            public void onFinish() {
            }
        });
    }

    private void backToPreviousView(){
        progressDialog.dismiss();
        Toast.makeText(getActivity(), "Perfil actualizado", Toast.LENGTH_LONG).show();
        userEditListener.onUserEditInfoSaved();
    }

    private boolean isAccountDataValid(){
        boolean nameEmpty = nameField.getText().toString().isEmpty() || nameField.getText().toString().length() == 0;
        boolean apellidoEmpty = lastNameField.getText().toString().isEmpty() || lastNameField.getText().toString().length() == 0;
        return !nameEmpty && !apellidoEmpty;
    }

    private Boolean isEmpty(EditText textField){
        return (textField.getText().length() == 0 || textField.getText().equals(""));
    }

    private Boolean isEmailValid(EditText textField){
        emailMatcher = emailVerificationPattern.matcher(textField.getText().toString());
        return emailMatcher.matches();
    }

    private ActionBar getActionBar() {
        return ((ActionBarActivity) getActivity()).getSupportActionBar();
    }

    private EditText nameField, lastNameField, emailField, usernameField, passwordField, passwordConfirmField;
    private Button saveUserInfoButton;
    private Model_User modelUser;
    private ProgressDialog progressDialog;
    private View mainUserInfoView;
    private Pattern emailVerificationPattern;
    private Matcher emailMatcher;
    private OnUserInfoFragmentInteractionListener userEditListener;
    private final int ACTIVE_STATUS = 1;
    private final String EDIT_USER_URL_API = Config.getInstance().MAIN_URL_API + "user/";
    private final String EDIT_CLIENT_URL_API = Config.getInstance().MAIN_URL_API + "client/";
    private final String VALIDATE_USERNAME_URL_API = Config.MAIN_URL_API + "validateUsername/";
    private final String VALIDATE_CLIENT_MAIL_URL_API = Config.MAIN_URL_API + "client/validateEmail/";
    private static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
}
