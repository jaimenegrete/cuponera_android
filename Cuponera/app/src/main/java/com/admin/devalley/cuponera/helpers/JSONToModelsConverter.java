package com.admin.devalley.cuponera.helpers;

import com.admin.devalley.cuponera.models.Model_Coupon;
import com.admin.devalley.cuponera.models.Model_Stablishment;
import com.admin.devalley.cuponera.models.Model_User;

import org.json.JSONException;
import org.json.JSONObject;

public class JSONToModelsConverter {

    public static Model_Stablishment convertJSONToStablishment(JSONObject stablishmentJSON){
        Model_Stablishment stablishment = null;
        try {
            stablishment = new Model_Stablishment(
                    stablishmentJSON.getInt("id"),
                    stablishmentJSON.getString("name"),
                    stablishmentJSON.getString("address"),
                    stablishmentJSON.getString("description"),
                    stablishmentJSON.getString("image"),
                    stablishmentJSON.getString("stablishmentTypeName"),
                    stablishmentJSON.getDouble("latitude"),
                    stablishmentJSON.getDouble("longitude"),
                    stablishmentJSON.getString("phone")
            );
        } catch (JSONException JSONException) {
            JSONException.printStackTrace();
        }
        return stablishment;
    }

    public static Model_Coupon convertJSONToCoupon(JSONObject couponJSON){
        Model_Coupon coupon = null;
        try{
            int downloaded = 0, acquired = 0;
            if (couponJSON.has("acquired")){
                downloaded = couponJSON.getInt("downloaded");
                acquired = couponJSON.getInt("acquired");
            }
            coupon = new Model_Coupon(
                    couponJSON.getInt("id"),
                    couponJSON.getString("name"),
                    Parser.stringToDate(couponJSON.getString("start_date")),
                    Parser.stringToDate(couponJSON.getString("end_date")),
                    couponJSON.getString("image"),
                    couponJSON.getString("description"),
                    true,
                    couponJSON.getString("qr_image"),
                    downloaded,
                    acquired,
                    couponJSON.getInt("used")
            );
            if (couponJSON.has("stablishment_id"))
                coupon.stablishment_id =  couponJSON.getInt("stablishment_id");
            if (couponJSON.has("stablishmentTypeName"))
                coupon.stablishmentType =  couponJSON.getString("stablishmentTypeName");

            if (couponJSON.has("coupon_id"))
                coupon.id =  couponJSON.getInt("coupon_id");
        }catch (JSONException JSONException){

            JSONException.printStackTrace();
        }
        return coupon;
    }

    public static Model_User convertJSONToUser(JSONObject userJSON){
        Model_User user = null;
        try{
            user = new Model_User(
                    userJSON.getInt("id"),
                    userJSON.getString("username"),
                    userJSON.getJSONObject("userType").getInt("id")
            );
        }catch(JSONException JSONException){
            JSONException.printStackTrace();
        }
        return user;
    }
}
