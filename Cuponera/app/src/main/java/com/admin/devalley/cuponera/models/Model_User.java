package com.admin.devalley.cuponera.models;

/**
 * Created by user on 02/03/2015.
 */
public class Model_User {

    public int id;
    public String username;
    public int userType;
    public int clientId;
    public int adminId;

    public Model_User(int id, String username, int userType){
        this.id = id;
        this.username = username;
        this.userType = userType;
    }

    public enum UserType {
        SUPERADMIN(1), ADMIN(2), CLIENT(3);

        public int id;

        UserType(int id) {
            this.id = id;
        }

    }

}
