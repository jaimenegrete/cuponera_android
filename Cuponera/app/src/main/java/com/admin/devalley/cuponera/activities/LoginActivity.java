package com.admin.devalley.cuponera.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.admin.devalley.cuponera.R;
import com.admin.devalley.cuponera.config.Config;
import com.admin.devalley.cuponera.models.Model_User;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class LoginActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginMainContext = this;
        getSupportActionBar().hide();

        userFieldLogin = (EditText)findViewById(R.id.userField);
        passFieldLogin = (EditText)findViewById(R.id.passField);

        Button loginBtn = (Button)findViewById(R.id.loginBtn);
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });

        Button registerBtn = (Button)findViewById(R.id.registerBtn);
        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToSignUp();
            }
        });
    }

    private void login(){

        if(isEditTextEmpty(userFieldLogin) || isEditTextEmpty(passFieldLogin)){
            Toast.makeText(loginMainContext, "Ingrese usuario y contraseña", Toast.LENGTH_LONG).show();
            return;
        }

        AsyncHttpClient asyncClient = new AsyncHttpClient();
        RequestParams requestParams = new RequestParams();
        requestParams.add("user", userFieldLogin.getText().toString());
        requestParams.add("pass", passFieldLogin.getText().toString());

        String url = LOGIN_URL_API + "/" + userFieldLogin.getText().toString() + "/" + passFieldLogin.getText().toString();
        asyncClient.get(url, requestParams, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode,
                                  org.apache.http.Header[] headers,
                                  java.lang.Throwable throwable,
                                  org.json.JSONObject errorResponse){
                Toast.makeText(loginMainContext,"Ocurrió un error en el servidor",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject json) {

                try {
                    Model_User user = new Model_User(
                            json.getInt("id"),
                            json.getString("username"),
                            json.getInt("user_type_id")
                    );
                    if(!json.getString("id").isEmpty()) {//Usuario valido

                        Config.getInstance().setUser(user);
                        if (user.userType == Model_User.UserType.ADMIN.id){ //Es admin
                            getAdminIdFromWebService(user);
                        }else {
                            if (user.userType == Model_User.UserType.CLIENT.id){ //Es cliente
                                getClientIdFromWebService(user);

                            }else
                                Toast.makeText(loginMainContext,"Tipo de usuario inexistente",Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        Toast.makeText(loginMainContext,"Nombre de usuario y/o contraseña incorrectos",Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException JSONException) {
                    JSONException.printStackTrace();
                    Toast.makeText(loginMainContext,"Ocurrió un error en el servidor",Toast.LENGTH_SHORT).show();
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

    private void getAdminIdFromWebService(Model_User user){

        AsyncHttpClient asyncClient = new AsyncHttpClient();

        String url = ADMIN_URL_API + user.id;
        asyncClient.get(url, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode,
                                  org.apache.http.Header[] headers,
                                  java.lang.Throwable throwable,
                                  org.json.JSONObject errorResponse){
                Toast.makeText(loginMainContext,"Ocurrió un error en el servidor",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject json) {
                try {
                    if(json.getInt("status") == ACTIVE_STATUS){//Usuario activo
                        Config.getInstance().getUser().adminId = json.getInt("id");
                        getStablishmentTypes();
                        Intent intent = new Intent(loginMainContext,AdminMainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                    else{
                        Toast.makeText(loginMainContext,"Tu cuenta ha sido desactivada",Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException JSONException) {
                    JSONException.printStackTrace();
                    Toast.makeText(loginMainContext,"Cliente Inválido",Toast.LENGTH_SHORT).show();
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

    private void getClientIdFromWebService(Model_User user){

        AsyncHttpClient asyncClient = new AsyncHttpClient();

        String url = CLIENT_URL_API + user.id;
        asyncClient.get(url, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode,
                                  org.apache.http.Header[] headers,
                                  java.lang.Throwable throwable,
                                  org.json.JSONObject errorResponse){
                Toast.makeText(loginMainContext,"Ocurrió un error en el servidor",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject json) {
                try {
                    if(json.getInt("status") == ACTIVE_STATUS) {//Usuario activo
                        Config.getInstance().getUser().clientId = json.getInt("id");
                        getStablishmentTypes();
                        Intent intent = new Intent(loginMainContext, ClientMainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                    else{
                        Toast.makeText(loginMainContext,"Tu cuenta ha sido desactivada",Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException JSONException) {
                    JSONException.printStackTrace();
                    Toast.makeText(loginMainContext,"Cliente Inválido",Toast.LENGTH_SHORT).show();
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }

            @Override
            public void onFinish() {

            }
        });
    }

    private void getStablishmentTypes(){
        AsyncHttpClient asyncClient = new AsyncHttpClient();

        String url = STABLISHMENT_TYPES_URL_API;
        asyncClient.get(url, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode,
                                  org.apache.http.Header[] headers,
                                  java.lang.Throwable throwable,
                                  org.json.JSONObject errorResponse){
                Toast.makeText(loginMainContext,"Ocurrió un error en el servidor",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray jsonStablishmentTypesArray) {
                ArrayList<String> stablishmentTypes = new ArrayList<String>();
                for (int i = 0; i < jsonStablishmentTypesArray.length(); i++) {//Llena el arreglo de tipos de establecimientos
                    try {
                        JSONObject stablishmentTypeJSONObject = jsonStablishmentTypesArray.getJSONObject(i);
                        String stablishmentTypeName = stablishmentTypeJSONObject.getString("name");
                        stablishmentTypes.add(stablishmentTypeName);
                    } catch (JSONException JSONException) {
                        JSONException.printStackTrace();
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                }
                Config.getInstance().setStablishmentTypes(stablishmentTypes);
            }

            @Override
            public void onFinish() {}
        });
    }

    private void goToSignUp(){
        Intent intent = new Intent(loginMainContext,RegisterActivity.class);
        startActivity(intent);
    };

    private Boolean isEditTextEmpty(EditText textField){
        return (textField.getText().length() == 0 || textField.getText().equals(""));
    }


    private EditText userFieldLogin;
    private EditText passFieldLogin;
    private Context loginMainContext;
    private final int ACTIVE_STATUS = 1;
    private final String LOGIN_URL_API = Config.MAIN_URL_API +"validateUser";
    private final String CLIENT_URL_API = Config.MAIN_URL_API +"clientByUserId/";
    private final String ADMIN_URL_API = Config.MAIN_URL_API +"stablishmentAdminByUserId/";
    private final String STABLISHMENT_TYPES_URL_API = Config.MAIN_URL_API + "stablishmentType/";
}
