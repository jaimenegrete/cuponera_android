package com.admin.devalley.cuponera.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.admin.devalley.cuponera.R;
import com.admin.devalley.cuponera.interfaces.OnContactDataRegistrationFragmentListener;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ContactDataRegistrationFragment extends Fragment {

    public static ContactDataRegistrationFragment newInstance() {
        return new ContactDataRegistrationFragment();
    }

    public ContactDataRegistrationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        emailVerificationPattern = Pattern.compile(EMAIL_PATTERN);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mainContactDataRegistrationView = inflater.inflate(R.layout.fragment_registro, container, false);

        nameField = (EditText) mainContactDataRegistrationView.findViewById(R.id.reg_nombreField);
        lastNameField = (EditText) mainContactDataRegistrationView.findViewById(R.id.reg_apellidoField);
        emailField = (EditText) mainContactDataRegistrationView.findViewById(R.id.reg_correoField);

        nextStepButton = (Button) mainContactDataRegistrationView.findViewById(R.id.continuarBtn);
        nextStepButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doNextStep();
            }
        });

        return mainContactDataRegistrationView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            onContactDataRegistrationFragmentListener = (OnContactDataRegistrationFragmentListener) activity;
        } catch (ClassCastException exception) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onContactDataRegistrationFragmentListener = null;
    }

    private void doNextStep(){
        if(isEmpty(nameField) || isEmpty(lastNameField)){
            Toast.makeText(getActivity(), "Llene todos los campos", Toast.LENGTH_LONG).show();
            return;
        }

        if (!isEmailValid(emailField)){
            Toast.makeText(getActivity(), "Proporcione un email válido", Toast.LENGTH_LONG).show();
            return;
        }

        onContactDataRegistrationFragmentListener.goToNextStep(
                NEXT_VIEW_POSITION,
                nameField.getText().toString(),
                lastNameField.getText().toString(),
                emailField.getText().toString()
        );
    }

    private Boolean isEmpty(EditText textField){
        return (textField.getText().length() == ZERO_CHARACTERS || textField.getText().equals(""));
    }

    private Boolean isEmailValid(EditText textField){
        emailMatcher = emailVerificationPattern.matcher(textField.getText().toString());
        return emailMatcher.matches();
    }

    private Button nextStepButton;
    private View mainContactDataRegistrationView;
    private EditText nameField, lastNameField, emailField;
    private Pattern emailVerificationPattern;
    private Matcher emailMatcher;
    private OnContactDataRegistrationFragmentListener onContactDataRegistrationFragmentListener;
    private final int NEXT_VIEW_POSITION = 1;
    private final int ZERO_CHARACTERS = 0;
    private static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
}
