package com.admin.devalley.cuponera.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.admin.devalley.cuponera.R;
import com.admin.devalley.cuponera.config.Config;
import com.admin.devalley.cuponera.helpers.JSONToModelsConverter;
import com.admin.devalley.cuponera.interfaces.OnCouponTransactionListener;
import com.admin.devalley.cuponera.interfaces.OnCouponsFragmentInteractionListener;
import com.admin.devalley.cuponera.listAdapters.MyCouponsAdapter;
import com.admin.devalley.cuponera.models.Model_Coupon;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.image.SmartImageView;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by user on 07/04/2015.
 */
public class MyCouponsFragment extends Fragment implements AbsListView.OnItemClickListener,
        AdapterView.OnItemSelectedListener,
        OnCouponTransactionListener {

    public static MyCouponsFragment newInstance() {
        return new MyCouponsFragment();
    }

    public MyCouponsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        couponsArray = new ArrayList<Model_Coupon>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getActionBar().setTitle("Mis Cupones");

        mainCouponsView = inflater.inflate(R.layout.fragment_coupons, container, false);
        couponsListView = (AbsListView) mainCouponsView.findViewById(android.R.id.list);
        couponsListView.setOnItemClickListener(this);
        Spinner stablishmentTypeSpinner = (Spinner) mainCouponsView.findViewById(R.id.stablishmentTypeSpinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.stablishmentsTypesArray, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        stablishmentTypeSpinner.setAdapter(adapter);
        stablishmentTypeSpinner.setOnItemSelectedListener(this);

        qrPanelLayout = (RelativeLayout)mainCouponsView.findViewById(R.id.qrPanelLayout);
        qrPanelLayout.setVisibility(View.INVISIBLE);

        qrImageView = (SmartImageView)mainCouponsView.findViewById(R.id.qrImageView);
        backButton = (Button)mainCouponsView.findViewById(R.id.backBtn);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                qrPanelLayout.setVisibility(View.INVISIBLE);
            }
        });

        searchCouponQuery = (EditText) mainCouponsView.findViewById(R.id.searchCouponField);
        searchCouponQuery.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable searchTextField) {
                String searchQuery = searchCouponQuery.getText().toString().toLowerCase(Locale.getDefault());
                filterCouponsBySearchQuery(searchQuery);
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int startPosition,int charCount, int textLengthChanged) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int startPosition, int textLengthBeforeChanged,int charCount) {}
        });
        getDataFromWebService();

        return mainCouponsView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (null != couponsListener) {
            // Notify the active callbacks interface (the activity, if the
            // fragment is attached to one) that an item has been selected.
            couponsListener.onMyCouponSelected(couponsArray.get(position));
        }else{
            Toast.makeText(getActivity(), "Listener es null", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            couponsListener = (OnCouponsFragmentInteractionListener) activity;
        } catch (ClassCastException exception) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        couponsListener = null;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String stablishmentCategory = parent.getItemAtPosition(position).toString();
        filterCouponsByCategoryId(stablishmentCategory);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void getDataFromWebService(){
        int clientId = Config.getInstance().getUser().clientId;
        AsyncHttpClient asyncClient = new AsyncHttpClient();
        RequestParams requestParams = new RequestParams();
        String url = COUPONS_URL_API + clientId;

        asyncClient.get(url, requestParams, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode,
                                  Header[] headers,
                                  Throwable throwable,
                                  JSONObject errorResponse){
                Toast.makeText(getActivity(), "Ocurrió un error en el servidor", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray jsonCouponsArray) {
                try {
                    final boolean THERE_ARE_COUPONS = jsonCouponsArray.length() > 0;
                    if (THERE_ARE_COUPONS){
                        couponsArray.clear();
                        for (int i = 0; i < jsonCouponsArray.length(); i++) {
                            JSONObject couponJSONObject = jsonCouponsArray.getJSONObject(i);
                            Model_Coupon coupon = JSONToModelsConverter.convertJSONToCoupon(couponJSONObject);
                            couponsArray.add(coupon);
                        }
                        refreshView(couponsArray);
                    }else{
                        Toast.makeText(getActivity(),"No tiene cupones",Toast.LENGTH_SHORT);
                    }

                } catch (JSONException JSONException) {
                    JSONException.printStackTrace();
                    Toast.makeText(getActivity(),"Cupón inválido",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFinish() {
            }
        });
    }

    private void refreshView(ArrayList<Model_Coupon> couponsArray){
        couponsAdapter = new MyCouponsAdapter(getActivity(),R.layout.my_coupon_list_layout, couponsArray, this);
        ((AdapterView<ListAdapter>) couponsListView).setAdapter(couponsAdapter);
    }

    private ActionBar getActionBar() {
        return ((ActionBarActivity) getActivity()).getSupportActionBar();
    }

    @Override
    public void onCouponAcquire(Model_Coupon modelCoupon) {

    }

    @Override
    public void onCouponClientRedeem(final Model_Coupon modelCoupon) {
        AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
        progressDialog = ProgressDialog.show(getActivity(), "Canjeando", "Espere por favor...", true);

        String url = REDEEM_COUPON_URL_API + modelCoupon.id + "/" + Config.getInstance().getUser().clientId;
        asyncHttpClient.get(url, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode,
                                  org.apache.http.Header[] headers,
                                  java.lang.Throwable throwable,
                                  org.json.JSONObject errorResponse) {
                Toast.makeText(getActivity(), "Ocurrió un error en el servidor", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject json) {
                try {
                    if(json.has("response")){
                        if (json.getBoolean("response")){
                            getDataFromWebService();
                            qrPanelLayout.setVisibility(View.VISIBLE);
                            qrImageView.setImageUrl(QR_IMAGE_URL_ROUTE + modelCoupon.qrImage);
                            Toast.makeText(getActivity(), "Cupón canjeado " + modelCoupon.id, Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getActivity(), "No se pudo canjear el cupón", Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        Toast.makeText(getActivity(), "No se pudo canjear el cupón", Toast.LENGTH_SHORT).show();
                    }
                    progressDialog.dismiss();
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }

            @Override
            public void onFinish() {
            }
        });
    }

    @Override
    public void onCouponAdminRedeem(Model_Coupon modelCoupon) {

    }

    private void filterCouponsByCategoryId(String stablishmentCategory){
        ArrayList<Model_Coupon> filteredCouponsArray = new ArrayList<>();
        for(Model_Coupon coupon: couponsArray){
            if(coupon.stablishmentType.equals(stablishmentCategory)){
                filteredCouponsArray.add(coupon);
            }
        }
        refreshView(filteredCouponsArray);
    }

    private void filterCouponsBySearchQuery(String query){
        couponsAdapter.getFilter().filter(query);
    }

    private OnCouponsFragmentInteractionListener couponsListener;
    private View mainCouponsView;
    private RelativeLayout qrPanelLayout;
    private Button backButton;
    private SmartImageView qrImageView;
    private MyCouponsAdapter couponsAdapter;
    private ArrayList<Model_Coupon> couponsArray;
    private AbsListView couponsListView;
    private ProgressDialog progressDialog;
    private EditText searchCouponQuery;
    private final String COUPONS_URL_API = Config.getInstance().MAIN_URL_API + "coupons/client/";
    private final String REDEEM_COUPON_URL_API = Config.getInstance().MAIN_URL_API + "coupon/exchangeClientCoupon/";
    private final String QR_IMAGE_URL_ROUTE = Config.getInstance().PUBLIC_URL_API + "coupon/qrimages/";

}
