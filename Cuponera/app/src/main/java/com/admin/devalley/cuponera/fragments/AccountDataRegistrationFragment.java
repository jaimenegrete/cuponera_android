package com.admin.devalley.cuponera.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.admin.devalley.cuponera.activities.LoginActivity;
import com.admin.devalley.cuponera.R;
import com.admin.devalley.cuponera.config.Config;
import com.admin.devalley.cuponera.helpers.JSONToModelsConverter;
import com.admin.devalley.cuponera.interfaces.OnAccountDataRegistrationInteractionListener;
import com.admin.devalley.cuponera.models.Model_User;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class AccountDataRegistrationFragment extends Fragment {

    public static AccountDataRegistrationFragment newInstance() {
        return new AccountDataRegistrationFragment();
    }

    public AccountDataRegistrationFragment() {
        // Required empty public constructor
        this.name = "";
        this.lastName = "";
        this.email = "";
    }

    public void setContactData(String name, String lastName, String email){
        this.name = name;
        this.lastName = lastName;
        this.email = email;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mainAccountDataRegistrationView = inflater.inflate(R.layout.fragment_registro2, container, false);

        usernameField = (EditText) mainAccountDataRegistrationView.findViewById(R.id.reg_userNameField);
        passwordField = (EditText) mainAccountDataRegistrationView.findViewById(R.id.reg_passField);
        passwordConfirmField = (EditText) mainAccountDataRegistrationView.findViewById(R.id.reg_passConfirmField);

        signupButton = (Button) mainAccountDataRegistrationView.findViewById(R.id.reg_registroBtn);
        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkFields();
            }
        });
        return mainAccountDataRegistrationView;
    }

    public void checkFields(){
        if(isEmpty(usernameField) || isEmpty(passwordField) || isEmpty(passwordConfirmField)){
            Toast.makeText(getActivity(), "Llene todos los campos", Toast.LENGTH_LONG).show();
        }else{
            if (isAccountDataValid()){
                if (passwordField.getText().toString().equals(passwordConfirmField.getText().toString()))
                    validateUserName();
                else
                    Toast.makeText(getActivity(), "Las contraseñas no coinciden", Toast.LENGTH_LONG).show();
            }else {
                onAccountDataRegistrationInteractionListener.goToFirstStep();
            }
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            onAccountDataRegistrationInteractionListener = (OnAccountDataRegistrationInteractionListener) activity;
        } catch (ClassCastException exception) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onAccountDataRegistrationInteractionListener = null;
    }

    private void sendAccountData(){

        AsyncHttpClient asyncHttpClient = new AsyncHttpClient();

        JSONObject jsonUser = new JSONObject();
        JSONObject jsonUserType = new JSONObject();
        try {
            jsonUser.put("username", usernameField.getText().toString());
            jsonUser.put("password", passwordField.getText().toString());
            jsonUserType.put("id", Model_User.UserType.CLIENT.id);
            jsonUser.put("userType", jsonUserType);
        } catch (JSONException JSONException) {
            JSONException.printStackTrace();
        }

        StringEntity entity = null;
        try {
            entity = new StringEntity(jsonUser.toString());
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
        } catch (UnsupportedEncodingException exception) {
            exception.printStackTrace();
        }

        progressDialog.setMessage("Creando usuario...");
        asyncHttpClient.post(getActivity(), CREATE_USER_URL_API, entity, "application/json", new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode,
                                  org.apache.http.Header[] headers,
                                  java.lang.Throwable throwable,
                                  org.json.JSONObject errorResponse){
                Toast.makeText(mainAccountDataRegistrationView.getContext(),"Ocurrió un error en el servidor",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject json) {

                try {
                    sendContactData(JSONToModelsConverter.convertJSONToUser(json));
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }

            @Override
            public void onFinish() {
            }
        });
    }

    private void validateUserName(){
        AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
        progressDialog = ProgressDialog.show(mainAccountDataRegistrationView.getContext(), "Enviando", "Validando nombre de usuario...", true);
        String url = VALIDATE_USERNAME_URL_API + usernameField.getText().toString();
        asyncHttpClient.get(url, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode,
                                  org.apache.http.Header[] headers,
                                  java.lang.Throwable throwable,
                                  org.json.JSONObject errorResponse) {
                Toast.makeText(mainAccountDataRegistrationView.getContext(), "Ocurrió un error en el servidor", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject json) {
                try {
                    if(json.has("error")){
                        validateClientMail();
                    }else{
                        Toast.makeText(mainAccountDataRegistrationView.getContext(), "El nombre de usuario ya existe", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }

            @Override
            public void onFinish() {
            }
        });
    }

    private void validateClientMail(){
        AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
        progressDialog.setMessage("Validando email...");
        String url = VALIDATE_CLIENT_MAIL_URL_API + email;
        asyncHttpClient.get(url, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode,
                                  org.apache.http.Header[] headers,
                                  java.lang.Throwable throwable,
                                  org.json.JSONObject errorResponse) {
                Toast.makeText(mainAccountDataRegistrationView.getContext(), "Ocurrió un error en el servidor", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject json) {
                try {
                    if(json.has("success")){
                        sendAccountData();
                    }else{
                        Toast.makeText(mainAccountDataRegistrationView.getContext(), "El email ya ha sido utilizado", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }

            @Override
            public void onFinish() {
            }
        });
    }

    private void sendContactData(Model_User user){
        AsyncHttpClient asyncHttpClient = new AsyncHttpClient();

        JSONObject jsonUser = new JSONObject();
        try {
            jsonUser.put("name", name);
            jsonUser.put("lastName", lastName);
            jsonUser.put("email", email);
            jsonUser.put("status", ACTIVE_STATUS);
        } catch (JSONException JSONException) {
            JSONException.printStackTrace();
        }

        StringEntity entity = null;
        try {
            entity = new StringEntity(jsonUser.toString());
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
        } catch (UnsupportedEncodingException exception) {
            exception.printStackTrace();
        }

        progressDialog.setMessage("Creando cuenta...");
        String url = CREATE_CLIENT_URL_API + user.id;
        asyncHttpClient.post(getActivity(),url, entity, "application/json", new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode,
                                  org.apache.http.Header[] headers,
                                  java.lang.Throwable throwable,
                                  org.json.JSONObject errorResponse){
                Toast.makeText(mainAccountDataRegistrationView.getContext(),"Ocurrió un error en el servidor",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject json) {

                try {
                    progressDialog.dismiss();

                    Toast.makeText(getActivity(), "Registro exitoso", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(getActivity(),LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);

                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }

            @Override
            public void onFinish() {
            }
        });
    }

    private boolean isAccountDataValid(){
        boolean nameEmpty = name.isEmpty() || name.length() == ZERO_CHARACTERS;
        boolean apellidoEmpty = lastName.isEmpty() || lastName.length() == ZERO_CHARACTERS;
        return !nameEmpty && !apellidoEmpty;
    }

    private Boolean isEmpty(EditText textField){
        return (textField.getText().length() == ZERO_CHARACTERS || textField.getText().equals(""));
    }

    private String name, lastName, email;
    private Button signupButton;
    private View mainAccountDataRegistrationView;
    private ProgressDialog progressDialog;
    private EditText usernameField, passwordField, passwordConfirmField;
    private OnAccountDataRegistrationInteractionListener onAccountDataRegistrationInteractionListener;
    private final int ZERO_CHARACTERS = 0;
    private final int ACTIVE_STATUS = 1;
    private final String CREATE_USER_URL_API = Config.MAIN_URL_API + "user";
    private final String CREATE_CLIENT_URL_API = Config.MAIN_URL_API + "client/";
    private final String VALIDATE_USERNAME_URL_API = Config.MAIN_URL_API + "validateUsername/";
    private final String VALIDATE_CLIENT_MAIL_URL_API = Config.MAIN_URL_API + "client/validateEmail/";

}
