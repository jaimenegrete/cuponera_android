package com.admin.devalley.cuponera.listAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.admin.devalley.cuponera.R;
import com.admin.devalley.cuponera.config.Config;
import com.admin.devalley.cuponera.models.Model_Stablishment;
import com.loopj.android.image.SmartImageView;

import java.util.List;

public class StablishmentsAdapter extends ArrayAdapter<Model_Stablishment> {

    public StablishmentsAdapter(Context context, int resourceId, List<Model_Stablishment> items) {
        super(context, resourceId, items);
        this.resourceId = resourceId;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        view = inflater.inflate(resourceId,parent, false);

        try{
            Model_Stablishment modelStablishment = getItem(position);
            ((TextView) view.findViewById(R.id.stabName)).setText(modelStablishment.name);
            ((SmartImageView) view.findViewById(R.id.stabListImage)).setImageUrl(URL_STABLISHMENT_IMAGE + modelStablishment.image);
            ((SmartImageView) view.findViewById(R.id.stabListImage)).setColorFilter(TRANSPARENT_BLACK);
            ((TextView) view.findViewById(R.id.stabDescription)).setText(modelStablishment.stablishmentType);

        }catch(Exception exception){
            exception.printStackTrace();
        }

        return view;
    }

    private int resourceId = 0;
    private LayoutInflater inflater;
    private Context context;
    private final int TRANSPARENT_BLACK = 0x64000000;
    private final String URL_STABLISHMENT_IMAGE = Config.getInstance().PUBLIC_URL_API + "stablishment/";

}
