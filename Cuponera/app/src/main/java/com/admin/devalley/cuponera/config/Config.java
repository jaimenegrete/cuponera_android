package com.admin.devalley.cuponera.config;

import android.content.SharedPreferences;

import com.admin.devalley.cuponera.models.Model_User;

import java.util.ArrayList;

public class Config {

    public static String MAIN_URL_API = "http://micuponera.x10.mx/api/";//URL del servidor
    public static String PUBLIC_URL_API = "http://micuponera.x10.mx/logos/";//URL de imagenes
    //public static String MAIN_URL_API = "http://192.168.1.163/cuponeraWeb/api/";//URL del servidor
    //public static String PUBLIC_URL_API = "http://192.168.1.163/cuponeraWeb/public/logos/";//URL de imagenes
    public static Config instance = null;
    public static Model_User modelUser;
    public static ArrayList<String> stablishmentTypesArray;

    public static Config getInstance(){
        if (instance == null){
            return new Config();
        }else
            return instance;
    }

    public void setUser(Model_User user){
        modelUser = user;
    }

    public Model_User getUser(){
        return modelUser;
    }

    public void setStablishmentTypes(ArrayList<String> stablishmentTypesArray){ this.stablishmentTypesArray = stablishmentTypesArray;}

    public ArrayList<String> getStablishmentTypes(){return stablishmentTypesArray; }
}
