package com.admin.devalley.cuponera.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.admin.devalley.cuponera.R;
import com.admin.devalley.cuponera.config.Config;
import com.admin.devalley.cuponera.helpers.JSONToModelsConverter;
import com.admin.devalley.cuponera.models.Model_Stablishment;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.image.SmartImageView;

import org.apache.http.Header;
import org.json.JSONObject;

/**
 * Created by cecia on 02/04/2015.
 */
public class StablishmentInfoFragment extends Fragment {

    public Model_Stablishment modelStablishment;
    public int stablishmentId;

    public static StablishmentInfoFragment newInstance(Model_Stablishment modelStablishment) {
        StablishmentInfoFragment fragment = new StablishmentInfoFragment();
        fragment.modelStablishment = modelStablishment;
        return fragment;
    }

    public static StablishmentInfoFragment newInstanceWithId(int stablishmentId) {
        StablishmentInfoFragment fragment = new StablishmentInfoFragment();
        fragment.stablishmentId = stablishmentId;
        return fragment;
    }

    public StablishmentInfoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mainStablishmentInfoView =  inflater.inflate(R.layout.fragment_info_stablishment, container, false);

        if (modelStablishment != null){
            refreshView();
        }else{
            getDataFromWebService();
        }
        return mainStablishmentInfoView;
    }

    private void getDataFromWebService(){
        AsyncHttpClient asyncClient = new AsyncHttpClient();
        RequestParams requestParams = new RequestParams();

        String url = URL_STABLISHMENT + stablishmentId;

        asyncClient.get(url, requestParams, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode,
                                  org.apache.http.Header[] headers,
                                  java.lang.Throwable throwable,
                                  org.json.JSONObject errorResponse){
                Toast.makeText(getActivity(), "Ocurrió un error en el servidor", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject jsonObject) {
                modelStablishment = JSONToModelsConverter.convertJSONToStablishment(jsonObject);
                refreshView();
            }

            @Override
            public void onFinish() {
            }
        });
    }

    private void refreshView(){
        getActionBar().setTitle(modelStablishment.name);
        stablishmentImageView = (SmartImageView)mainStablishmentInfoView.findViewById(R.id.imageView);
        stablishmentImageView.setImageUrl(URL_STABLISHMENT_IMAGE + modelStablishment.image);

        addressTxtView = (TextView)mainStablishmentInfoView.findViewById(R.id.adressTxtView);
        addressTxtView.setText(modelStablishment.address);
        phoneTxtView = (TextView)mainStablishmentInfoView.findViewById(R.id.phoneTxtView);
        phoneTxtView.setText(modelStablishment.phone);
        descriptionTxtView = (TextView)mainStablishmentInfoView.findViewById(R.id.detailsTxtView);
        descriptionTxtView.setText(modelStablishment.description);
    }

    private ActionBar getActionBar() {
        return ((ActionBarActivity) getActivity()).getSupportActionBar();
    }

    private View mainStablishmentInfoView;
    private SmartImageView stablishmentImageView;
    private TextView addressTxtView, phoneTxtView, descriptionTxtView;
    private final String URL_STABLISHMENT = Config.MAIN_URL_API + "stablishment/";
    private final String URL_STABLISHMENT_IMAGE = Config.getInstance().PUBLIC_URL_API + "stablishment/";

}
