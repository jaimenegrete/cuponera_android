package com.admin.devalley.cuponera.interfaces;

import com.admin.devalley.cuponera.models.Model_Stablishment;

/**
 * Created by user on 26/03/2015.
 */
public interface OnStablishmentsFragmentInteractionListener {
    public void onStablishmentSelected(Model_Stablishment modelStablishment);
}
