package com.admin.devalley.cuponera.interfaces;

import com.admin.devalley.cuponera.models.Model_Stablishment;

/**
 * Created by cecia on 02/04/2015.
 */
public interface OnCouponFragmentInteractionListener{

    public void onShowStablishmentSelected(Model_Stablishment modelStablishment);

    public void onShowStablishmentSelectedWithId(int stablismentId);
}
