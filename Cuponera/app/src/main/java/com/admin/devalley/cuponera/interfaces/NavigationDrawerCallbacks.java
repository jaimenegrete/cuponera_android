package com.admin.devalley.cuponera.interfaces;

/**
 * Created by user on 26/03/2015.
 */
public interface NavigationDrawerCallbacks {
    void onNavigationDrawerItemSelected(int position);
}
