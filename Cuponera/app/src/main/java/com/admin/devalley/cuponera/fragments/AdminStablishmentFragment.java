package com.admin.devalley.cuponera.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.Toast;

import com.admin.devalley.cuponera.R;
import com.admin.devalley.cuponera.config.Config;
import com.admin.devalley.cuponera.helpers.JSONToModelsConverter;
import com.admin.devalley.cuponera.interfaces.OnStablishmentFragmentInteractionListener;
import com.admin.devalley.cuponera.listAdapters.AdminCouponsAdapter;
import com.admin.devalley.cuponera.models.Model_Coupon;
import com.admin.devalley.cuponera.models.Model_Stablishment;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by user on 07/04/2015.
 */
public class AdminStablishmentFragment extends Fragment implements AbsListView.OnItemClickListener {
    public Model_Stablishment modelStablishment;

    public static AdminStablishmentFragment newInstance(Model_Stablishment modelStablishment) {
        AdminStablishmentFragment fragment = new AdminStablishmentFragment();
        fragment.modelStablishment = modelStablishment;
        return fragment;
    }

    public AdminStablishmentFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getActionBar().setTitle(modelStablishment.name);
        mainStablishmentView =  inflater.inflate(R.layout.fragment_establecimiento, container, false);

        couponsListView = (AbsListView) mainStablishmentView.findViewById(android.R.id.list);
        couponsListView.setOnItemClickListener(this);
        coupons = new ArrayList<>();
        getDataFromWebService();
        return mainStablishmentView;
    }



    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (null != stablishmentListener) {
            // Notify the active callbacks interface (the activity, if the
            // fragment is attached to one) that an item has been selected.
            stablishmentListener.onCouponSelected(coupons.get(position),modelStablishment);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            stablishmentListener = (OnStablishmentFragmentInteractionListener) activity;
        } catch (ClassCastException exception) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        stablishmentListener = null;
    }

    private void getDataFromWebService(){
        AsyncHttpClient asyncClient = new AsyncHttpClient();
        RequestParams requestParams = new RequestParams();

        String url = Config.MAIN_URL_API + COUPONS_URL_API + modelStablishment.id;

        asyncClient.get(url, requestParams, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode,
                                  org.apache.http.Header[] headers,
                                  java.lang.Throwable throwable,
                                  org.json.JSONObject errorResponse){
                Toast.makeText(getActivity(), "Ocurrió un error en el servidor", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray json) {

                try {
                    for (int i = 0; i < json.length(); i++){
                        JSONObject couponJSON = json.getJSONObject(i);
                        coupons.add(JSONToModelsConverter.convertJSONToCoupon(couponJSON));
                    }
                    refreshView();
                } catch (JSONException JSONException) {
                    JSONException.printStackTrace();
                }
            }

            @Override
            public void onFinish() {
            }
        });
    }

    private void refreshView(){
        adminCouponsAdapter = new AdminCouponsAdapter(getActivity(),R.layout.admin_coupon_stats_layout, coupons);
        ((AdapterView<ListAdapter>) couponsListView).setAdapter(adminCouponsAdapter);
    }

    private ActionBar getActionBar() {
        return ((ActionBarActivity) getActivity()).getSupportActionBar();
    }

    private OnStablishmentFragmentInteractionListener stablishmentListener;
    private ArrayList<Model_Coupon> coupons;
    private AbsListView couponsListView;
    private AdminCouponsAdapter adminCouponsAdapter;
    private View mainStablishmentView;
    private final String COUPONS_URL_API = "coupons/";
    private static final String ARG_PARAM_STABLISHMENT = "stablishment";

}
