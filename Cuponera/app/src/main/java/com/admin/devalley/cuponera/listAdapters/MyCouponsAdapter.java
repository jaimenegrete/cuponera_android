package com.admin.devalley.cuponera.listAdapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.TextView;

import com.admin.devalley.cuponera.R;
import com.admin.devalley.cuponera.config.Config;
import com.admin.devalley.cuponera.interfaces.OnCouponTransactionListener;
import com.admin.devalley.cuponera.models.Model_Coupon;
import com.loopj.android.image.SmartImageView;

import java.util.ArrayList;
import java.util.List;

public class MyCouponsAdapter extends ArrayAdapter<Model_Coupon> {

    public MyCouponsAdapter(Context context, int resourceId, List<Model_Coupon> items, OnCouponTransactionListener listener) {
        super(context, resourceId, items);
        this.resourceId = resourceId;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.originalCouponArray = new ArrayList<Model_Coupon>(items);
        this.filteredCouponArray = new ArrayList<Model_Coupon>(items);
        this.context = context;
        this.couponsListener = listener;
    }

    @Override
    public Filter getFilter(){
        if(filter == null){
            filter = new CouponsAdapterFilter();
        }
        return filter;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        view = inflater.inflate(resourceId,parent, false);

        try{
            final Model_Coupon modelCoupon = filteredCouponArray.get(position);
            ((TextView) view.findViewById(R.id.couponName)).setText(modelCoupon.name);

            String imageUrl = URL_IMAGE + modelCoupon.image;
            ((SmartImageView) view.findViewById(R.id.couponImage)).setImageUrl(imageUrl);
            ((SmartImageView) view.findViewById(R.id.couponImage)).setColorFilter(TRANSPARENT_BLACK);
            ((TextView) view.findViewById(R.id.couponDescription)).setText(modelCoupon.stablishmentType);

            Button acquireCouponButton = (Button)view.findViewById(R.id.redeemCouponButton);
            if (modelCoupon.used > 0){
                acquireCouponButton.setVisibility(View.INVISIBLE);
            }else{
                acquireCouponButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showConfirmRedeemDialog(modelCoupon);
                    }
                });
            }
        }catch(Exception exception){
            exception.printStackTrace();
        }

        return view;
    }

    private void showConfirmRedeemDialog(final Model_Coupon modelCoupon){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setMessage("¿Desea canjear el cupón?\nEsta acción no puede deshacerse.")
                .setTitle("Canjeo de cupón");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
                couponsListener.onCouponClientRedeem(modelCoupon);
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private class CouponsAdapterFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence searchQuery){
            FilterResults results = new FilterResults();

            //If there's nothing to filter on, return the original data for your list
            if(searchQuery == null || searchQuery.length() == 0)
            {
                results.values = originalCouponArray;
                results.count = originalCouponArray.size();
            }
            else
            {
                ArrayList<Model_Coupon> searchResultsArray = new ArrayList<Model_Coupon>();
                for(Model_Coupon coupon : originalCouponArray){
                    String couponName = coupon.name.toLowerCase();
                    if(couponName.contains(searchQuery)){
                        searchResultsArray.add(coupon);
                    }
                }
                results.values = searchResultsArray;
                results.count = filteredCouponArray.size();
            }

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence searchQuery, FilterResults filterResults)
        {
            filteredCouponArray = (ArrayList<Model_Coupon>)filterResults.values;
            clear();
            for(Model_Coupon coupon: filteredCouponArray){
                add(coupon);
            }
        }
    }
    private int resourceId = 0;
    private LayoutInflater inflater;
    private Context context;
    private Filter filter;
    private ArrayList<Model_Coupon> originalCouponArray;
    private ArrayList<Model_Coupon> filteredCouponArray;
    private OnCouponTransactionListener couponsListener;
    private final int TRANSPARENT_BLACK = 0x64000000;
    private final String URL_IMAGE = Config.getInstance().PUBLIC_URL_API + "coupon/";
}
