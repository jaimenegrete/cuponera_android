package com.admin.devalley.cuponera.helpers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by cecia on 31/03/2015.
 */
public class Parser {

    public static Date stringToDate(String dateString){
        Date date = null;
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
            date = format.parse(dateString);
        } catch (ParseException exception) {
            // TODO Auto-generated catch block
            
            exception.printStackTrace();
        }
        return date;
    }
}
