package com.admin.devalley.cuponera.interfaces;

/**
 * Created by user on 07/04/2015.
 */
public interface OnUserInfoFragmentInteractionListener {
    public void onEditUserInfoSelected(String name, String lastName, String email);

    public void onUserEditInfoSaved();
}
