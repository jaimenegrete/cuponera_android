package com.admin.devalley.cuponera.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.app.Activity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.admin.devalley.cuponera.activities.LoginActivity;
import com.admin.devalley.cuponera.R;
import com.admin.devalley.cuponera.config.Config;
import com.admin.devalley.cuponera.interfaces.NavigationDrawerCallbacks;

/**
 * Fragment used for managing interactions for and presentation of a navigation drawer.
 * See the <a href="https://developer.android.com/design/patterns/navigation-drawer.html#Interaction">
 * design guidelines</a> for a complete explanation of the behaviors implemented here.
 */
public class AdminNavigationDrawerFragment extends Fragment {

    public AdminNavigationDrawerFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Read in the flag indicating whether or not the user has demonstrated awareness of the
        // drawer. See PREF_USER_LEARNED_DRAWER for details.
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        hasUserLearnedDrawer = sharedPreferences.getBoolean(PREF_USER_LEARNED_DRAWER, false);

        if (savedInstanceState != null) {
            currentSelectedPosition = savedInstanceState.getInt(STATE_SELECTED_POSITION);
            isFromSavedInstanceState = true;
        }

        // Select either the default item (0) or the last selected item.
        selectAdminDrawerPosition(currentSelectedPosition);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Indicate that this fragment would like to influence the set of actions in the action bar.
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mainAdminNavigationDrawerView = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);

        userInfo = mainAdminNavigationDrawerView.findViewById(R.id.userInfoView);
        userInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectAdminDrawerPosition(EDIT_PROFILE_POSITION);
            }
        });
        userNameTxt = (TextView) mainAdminNavigationDrawerView.findViewById(R.id.userTxt);
        userNameTxt.setText(Config.getInstance().getUser().username);
        stablishmentsBtn = (Button) mainAdminNavigationDrawerView.findViewById(R.id.sec_stabBtn);
        stablishmentsBtn.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectAdminDrawerPosition(MY_STABLISHMENTS_POSITION);
            }
        });
        scannerBtn = (Button) mainAdminNavigationDrawerView.findViewById(R.id.secc_lectorBtn);
        scannerBtn.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectAdminDrawerPosition(READER_COUPONS_POSITION);
            }
        });

        logoutBtn = (Button) mainAdminNavigationDrawerView.findViewById(R.id.logoutBtn);
        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showLogoutDialog();
            }
        });
        return mainAdminNavigationDrawerView;
    }

    public boolean isDrawerOpen() {
        return adminNavigationDrawerLayout != null && adminNavigationDrawerLayout.isDrawerOpen(adminNavigationDrawerContainerView);
    }

    public void initializeAdminNavigationDrawerFragmentFromActivity(int fragmentId, DrawerLayout drawerLayout) {
        adminNavigationDrawerContainerView = getActivity().findViewById(fragmentId);
        adminNavigationDrawerLayout = drawerLayout;

        // set a custom shadow that overlays the main content when the drawer opens
        adminNavigationDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener

        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the navigation drawer and the action bar app icon.
        adminNavigationDrawerToggle = new ActionBarDrawerToggle(
                getActivity(),                    /* host Activity */
                adminNavigationDrawerLayout,                    /* DrawerLayout object */
                R.drawable.ic_drawer,             /* nav drawer image to replace 'Up' caret */
                R.string.navigation_drawer_open,  /* "open drawer" description for accessibility */
                R.string.navigation_drawer_close  /* "close drawer" description for accessibility */
        ) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                if (!isAdded()) {
                    return;
                }
                getActivity().supportInvalidateOptionsMenu(); // calls onPrepareOptionsMenu()
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (!isAdded()) {
                    return;
                }

                if (!hasUserLearnedDrawer) {
                    // The user manually opened the drawer; store this flag to prevent auto-showing
                    // the navigation drawer automatically in the future.
                    hasUserLearnedDrawer = true;
                    SharedPreferences sharedPreferences = PreferenceManager
                            .getDefaultSharedPreferences(getActivity());
                    sharedPreferences.edit().putBoolean(PREF_USER_LEARNED_DRAWER, true).apply();
                }

                getActivity().supportInvalidateOptionsMenu(); // calls onPrepareOptionsMenu()
            }
        };

        // If the user hasn't 'learned' about the drawer, open it to introduce them to the drawer,
        // per the navigation drawer design guidelines.
        if (!hasUserLearnedDrawer && !isFromSavedInstanceState) {
            adminNavigationDrawerLayout.openDrawer(adminNavigationDrawerContainerView);
        }

        // Defer code dependent on restoration of previous instance state.
        adminNavigationDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                adminNavigationDrawerToggle.syncState();
            }
        });

        adminNavigationDrawerLayout.setDrawerListener(adminNavigationDrawerToggle);
    }

    private void selectAdminDrawerPosition(int position) {
        currentSelectedPosition = position;

        if (adminNavigationDrawerLayout != null) {
            adminNavigationDrawerLayout.closeDrawer(adminNavigationDrawerContainerView);
        }
        if (adminNavigationDrawerCallbacks != null) {
            adminNavigationDrawerCallbacks.onNavigationDrawerItemSelected(position);//Con esto le paso la posicion al activity
        }
    }

    private void showLogoutDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage("¿Desea cerrar su sesion?")
                .setTitle("Cierre de sesion");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
                Config.getInstance().setUser(null);
                Intent intent = new Intent(getActivity(),LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            adminNavigationDrawerCallbacks = (NavigationDrawerCallbacks) activity;
        } catch (ClassCastException exception) {
            throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        adminNavigationDrawerCallbacks = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_SELECTED_POSITION, currentSelectedPosition);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Forward the new configuration the drawer toggle component.
        adminNavigationDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // If the drawer is open, show the global app actions in the action bar. See also
        // showGlobalContextActionBar, which controls the top-left area of the action bar.
        if (adminNavigationDrawerLayout != null && isDrawerOpen()) {
            inflater.inflate(R.menu.global, menu);
            showGlobalContextActionBar();
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (adminNavigationDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        if (item.getItemId() == R.id.action_example) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showGlobalContextActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        //actionBar.setTitle(R.string.app_name);
    }

    private ActionBar getActionBar() {
        return ((ActionBarActivity) getActivity()).getSupportActionBar();
    }

    /**
     * Remember the position of the selected item.
     */
    private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";

    /**
     * Per the design guidelines, you should show the drawer on launch until the user manually
     * expands it. This shared preference tracks this.
     */
    private static final String PREF_USER_LEARNED_DRAWER = "navigation_drawer_learned";

    /**
     * A pointer to the current callbacks instance (the Activity).
     */
    private NavigationDrawerCallbacks adminNavigationDrawerCallbacks;

    /**
     * Helper component that ties the action bar to the navigation drawer.
     */
    private ActionBarDrawerToggle adminNavigationDrawerToggle;

    private DrawerLayout adminNavigationDrawerLayout;
    //private ListView mDrawerListView;
    private View adminNavigationDrawerContainerView;
    private View userInfo;
    private View mainAdminNavigationDrawerView;
    private TextView userNameTxt;
    private Button stablishmentsBtn, scannerBtn, logoutBtn;
    private int currentSelectedPosition = 1;
    private boolean isFromSavedInstanceState;
    private boolean hasUserLearnedDrawer;
    private final int EDIT_PROFILE_POSITION = 0;
    private final int MY_STABLISHMENTS_POSITION = 1;
    private final int READER_COUPONS_POSITION = 2;
}
