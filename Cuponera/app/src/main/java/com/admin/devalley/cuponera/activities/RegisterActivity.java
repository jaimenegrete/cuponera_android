package com.admin.devalley.cuponera.activities;

import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.widget.Toast;

import com.admin.devalley.cuponera.R;
import com.admin.devalley.cuponera.fragments.AccountDataRegistrationFragment;
import com.admin.devalley.cuponera.fragments.ContactDataRegistrationFragment;
import com.admin.devalley.cuponera.interfaces.OnAccountDataRegistrationInteractionListener;
import com.admin.devalley.cuponera.interfaces.OnContactDataRegistrationFragmentListener;


public class RegisterActivity extends ActionBarActivity
        implements OnContactDataRegistrationFragmentListener,
        OnAccountDataRegistrationInteractionListener {

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_registro, menu);
        return false;
    }

    @Override
    public void goToNextStep(int position, String name, String lastName, String email) {
        mViewPager.setCurrentItem( position );
        fragmentDatosCuenta.setContactData(name, lastName, email);
    }

    @Override
    public void onBackPressed() {
        if (mViewPager.getCurrentItem() == 0) {
            super.onBackPressed();
        } else {
            mViewPager.setCurrentItem(mViewPager.getCurrentItem() - 1);
        }
    }

    @Override
    public void goToFirstStep() {
        Toast.makeText(this, "Llene todos los campos", Toast.LENGTH_LONG).show();
        mViewPager.setCurrentItem( 0 );
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 0:
                    return fragmentDatosContacto;
                case 1:
                    return fragmentDatosCuenta;
            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(sectionsPagerAdapter);

        getSupportActionBar().hide();

        fragmentDatosContacto = ContactDataRegistrationFragment.newInstance();
        fragmentDatosCuenta = AccountDataRegistrationFragment.newInstance();
    }

    private SectionsPagerAdapter sectionsPagerAdapter;
    private ViewPager mViewPager;
    private AccountDataRegistrationFragment fragmentDatosCuenta;
    private ContactDataRegistrationFragment fragmentDatosContacto;

}
