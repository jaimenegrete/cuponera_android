package com.admin.devalley.cuponera.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.Toast;

import com.admin.devalley.cuponera.R;
import com.admin.devalley.cuponera.config.Config;
import com.admin.devalley.cuponera.helpers.JSONToModelsConverter;
import com.admin.devalley.cuponera.interfaces.OnCouponTransactionListener;
import com.admin.devalley.cuponera.interfaces.OnStablishmentFragmentInteractionListener;
import com.admin.devalley.cuponera.listAdapters.ClientCouponsAdapter;
import com.admin.devalley.cuponera.models.Model_Coupon;
import com.admin.devalley.cuponera.models.Model_Stablishment;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class StablishmentFragment extends Fragment implements AbsListView.OnItemClickListener,
        OnCouponTransactionListener {

    public Model_Stablishment modelStablishment;

    public static StablishmentFragment newInstance(Model_Stablishment modelStablishment) {
        StablishmentFragment fragment = new StablishmentFragment();
        fragment.modelStablishment = modelStablishment;
        return fragment;
    }

    public StablishmentFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getActionBar().setTitle(modelStablishment.name);
        mainStablishmentView =  inflater.inflate(R.layout.fragment_establecimiento, container, false);

        couponsListView = (AbsListView) mainStablishmentView.findViewById(android.R.id.list);
        couponsListView.setOnItemClickListener(this);
        coupons = new ArrayList<>();
        getDataFromWebService();
        return mainStablishmentView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (null != stablishmentListener) {
            // Notify the active callbacks interface (the activity, if the
            // fragment is attached to one) that an item has been selected.
            coupons.get(position).used = zeroCoupons;
            stablishmentListener.onCouponSelected(coupons.get(position),modelStablishment);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            stablishmentListener = (OnStablishmentFragmentInteractionListener) activity;
        } catch (ClassCastException exception) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        stablishmentListener = null;
    }

    private void getDataFromWebService(){
        AsyncHttpClient asyncClient = new AsyncHttpClient();
        RequestParams requestParams = new RequestParams();

        String url = Config.MAIN_URL_API + COUPONS_URL_API + modelStablishment.id;

        asyncClient.get(url, requestParams, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode,
                                  org.apache.http.Header[] headers,
                                  java.lang.Throwable throwable,
                                  org.json.JSONObject errorResponse){
                Toast.makeText(getActivity(), "Ocurrió un error en el servidor", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray json) {

                try {
                    for (int i = 0; i < json.length(); i++){
                        JSONObject couponJSON = json.getJSONObject(i);
                        coupons.add(JSONToModelsConverter.convertJSONToCoupon(couponJSON));
                    }
                    refreshView();
                } catch (JSONException JSONException) {
                    JSONException.printStackTrace();
                }
            }

            @Override
            public void onFinish() {
            }
        });
    }

    private void refreshView(){
        clientCouponsAdapter = new ClientCouponsAdapter(getActivity(),R.layout.coupon_list_layout, coupons, this);
        ((AdapterView<ListAdapter>) couponsListView).setAdapter(clientCouponsAdapter);
    }

    private ActionBar getActionBar() {
        return ((ActionBarActivity) getActivity()).getSupportActionBar();
    }

    @Override
    public void onCouponAcquire(final Model_Coupon modelCoupon) {
        AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
        progressDialog = ProgressDialog.show(getActivity(), "Adquiriendo", "Espere por favor...", true);
        String url = ACQUIRE_COUPON_URL_API + modelCoupon.id + "/" + Config.getInstance().getUser().clientId;
        asyncHttpClient.get(url, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode,
                                  org.apache.http.Header[] headers,
                                  java.lang.Throwable throwable,
                                  org.json.JSONObject errorResponse) {
                Toast.makeText(getActivity(), "Ocurrió un error en el servidor", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject json) {
                try {
                    if(json.has("response")){
                        if (json.getBoolean("response")){
                            coupons.remove(modelCoupon);
                            refreshView();
                            Toast.makeText(getActivity(), "Cupón adquirido", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getActivity(), "No se pudo adquirir el cupón", Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        Toast.makeText(getActivity(), "No se pudo adquirir el cupón", Toast.LENGTH_SHORT).show();
                    }
                    progressDialog.dismiss();
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }

            @Override
            public void onFinish() {
            }
        });
    }

    @Override
    public void onCouponClientRedeem(Model_Coupon modelCoupon) {

    }

    @Override
    public void onCouponAdminRedeem(Model_Coupon modelCoupon) {

    }

    private OnStablishmentFragmentInteractionListener stablishmentListener;
    private ArrayList<Model_Coupon> coupons;
    private AbsListView couponsListView;
    private ClientCouponsAdapter clientCouponsAdapter;
    private View mainStablishmentView;
    private ProgressDialog progressDialog;
    private final int zeroCoupons = 0;
    private final String COUPONS_URL_API = "coupons/";
    private final String ACQUIRE_COUPON_URL_API = Config.getInstance().MAIN_URL_API + "coupon/addClientCoupon/";
    private static final String ARG_PARAM_STABLISHMENT = "stablishment";
}
