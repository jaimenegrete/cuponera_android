package com.admin.devalley.cuponera.interfaces;

import com.admin.devalley.cuponera.models.Model_Coupon;

/**
 * Created by user on 26/03/2015.
 */
public interface OnCouponsFragmentInteractionListener {
    public void onCouponSelected(Model_Coupon modelCoupon);
    public void onMyCouponSelected(Model_Coupon modelCoupon);
}
