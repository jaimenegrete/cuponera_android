package com.admin.devalley.cuponera.interfaces;

import com.admin.devalley.cuponera.models.Model_Coupon;
import com.admin.devalley.cuponera.models.Model_Stablishment;

/**
 * Created by user on 26/03/2015.
 */
public interface OnStablishmentFragmentInteractionListener {
    public void onCouponSelected(Model_Coupon coupon, Model_Stablishment stablishment);
}
