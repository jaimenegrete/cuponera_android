package com.admin.devalley.cuponera.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.admin.devalley.cuponera.R;
import com.admin.devalley.cuponera.config.Config;
import com.admin.devalley.cuponera.interfaces.OnCouponFragmentInteractionListener;
import com.admin.devalley.cuponera.models.Model_Coupon;
import com.admin.devalley.cuponera.models.Model_Stablishment;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.image.SmartImageView;

import org.apache.http.Header;
import org.json.JSONObject;

public class CouponFragment extends Fragment {

    public Model_Coupon modelCoupon;
    public Model_Stablishment modelStablishment;

    public static CouponFragment newInstance(Model_Coupon modelCoupon, Model_Stablishment modelStablishment) {
        CouponFragment fragment = new CouponFragment();
        fragment.modelCoupon = modelCoupon;
        fragment.modelStablishment = modelStablishment;
        return fragment;
    }

    public CouponFragment(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            couponListener = (OnCouponFragmentInteractionListener) activity;
        } catch (ClassCastException exception) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getActionBar().setTitle(modelCoupon.name);
        mainCouponView =  inflater.inflate(R.layout.fragment_coupon, container, false);
        qrImageView = (SmartImageView)mainCouponView.findViewById(R.id.qrImageView);
        qrImageView.setImageUrl(QR_IMAGE_URL_ROUTE + modelCoupon.qrImage);
        qrImageView.setVisibility(View.INVISIBLE);

        couponImageView = (SmartImageView)mainCouponView.findViewById(R.id.couponImageView);
        couponImageView.setImageUrl(COUPON_IMAGE_URL_ROUTE + modelCoupon.image);

        ((TextView)mainCouponView.findViewById(R.id.couponDescription)).setText(modelCoupon.description);

        hideQRBtn = (Button)mainCouponView.findViewById(R.id.hideQRBtn);
        hideQRBtn.setVisibility(View.INVISIBLE);
        hideQRBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        acquireRedeemCouponBtn = (Button)mainCouponView.findViewById(R.id.comprarBtn);

        if(modelCoupon.used > zeroCoupons){
            acquireRedeemCouponBtn.setVisibility(View.GONE);
        }else{
            if (modelCoupon.downloaded < modelCoupon.acquired){
                acquireRedeemCouponBtn.setText("Adquirir");
                acquireRedeemCouponBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showConfirmAcquireDialog();
                    }
                });
            }
        }

        showStablishmentBtn = (Button)mainCouponView.findViewById(R.id.verStablishBtn);
        showStablishmentBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (modelStablishment != null)
                    couponListener.onShowStablishmentSelected(modelStablishment);
                else
                    couponListener.onShowStablishmentSelectedWithId(modelCoupon.stablishment_id);
            }
        });
        return mainCouponView;
    }

    public void acquireCoupon() {
        AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
        progressDialog = ProgressDialog.show(getActivity(), "Adquiriendo", "Espere por favor...", true);
        String url = ACQUIRE_COUPON_URL_API + modelCoupon.id + "/" + Config.getInstance().getUser().clientId;
        asyncHttpClient.get(url, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode,
                                  org.apache.http.Header[] headers,
                                  java.lang.Throwable throwable,
                                  org.json.JSONObject errorResponse) {
                Toast.makeText(getActivity(), "Ocurrió un error en el servidor", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject json) {
                try {
                    if(json.has("response")){
                        if (json.getBoolean("response")){

                            Toast.makeText(getActivity(), "Cupón adquirido", Toast.LENGTH_SHORT).show();
                            getActivity().getSupportFragmentManager().popBackStack();
                        }else{
                            Toast.makeText(getActivity(), "No se pudo adquirir el cupón", Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        Toast.makeText(getActivity(), "No se pudo adquirir el cupón", Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }

            @Override
            public void onFinish() {
                progressDialog.dismiss();
            }
        });
    }

    private void showConfirmAcquireDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage("¿Desea adquirir el cupón?")
                .setTitle("Adquirir cupón");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
                acquireCoupon();
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        couponListener = null;
    }

    private ActionBar getActionBar() {
        return ((ActionBarActivity) getActivity()).getSupportActionBar();
    }

    private View mainCouponView;
    private final int zeroCoupons = 0;
    private ProgressDialog progressDialog;
    private SmartImageView qrImageView, couponImageView;
    private Button acquireRedeemCouponBtn, hideQRBtn, showStablishmentBtn;
    private OnCouponFragmentInteractionListener couponListener;
    private final String QR_IMAGE_URL_ROUTE = Config.getInstance().PUBLIC_URL_API + "coupon/qrimages/";
    private final String ACQUIRE_COUPON_URL_API = Config.getInstance().MAIN_URL_API + "coupon/addClientCoupon/";
    private final String COUPON_IMAGE_URL_ROUTE = Config.getInstance().PUBLIC_URL_API + "coupon/";

}
