package com.admin.devalley.cuponera.listAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.admin.devalley.cuponera.R;
import com.admin.devalley.cuponera.config.Config;
import com.admin.devalley.cuponera.models.Model_Coupon;
import com.loopj.android.image.SmartImageView;

import java.util.List;

/**
 * Created by user on 26/03/2015.
 */
public class AdminCouponsAdapter extends ArrayAdapter<Model_Coupon> {

    public AdminCouponsAdapter(Context context, int resourceId, List<Model_Coupon> items) {
        super(context, resourceId, items);
        this.resourceId = resourceId;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        view = inflater.inflate(resourceId,parent, false);

        try{
            final Model_Coupon modelCoupon = getItem(position);
            ((TextView) view.findViewById(R.id.couponName)).setText(modelCoupon.name);

            String imageUrl = URL_IMAGE + modelCoupon.image;
            ((SmartImageView) view.findViewById(R.id.couponImage)).setImageUrl(imageUrl);

            ((TextView) view.findViewById(R.id.couponStats))
                    .setText("Adquiridos:" + modelCoupon.acquired +
                                    "  Descargados:" + modelCoupon.downloaded +
                                    "  Usados:" + modelCoupon.used

                    );


        }catch(Exception exception){
            exception.printStackTrace();
        }

        return view;
    }

    private int resourceId = 0;
    private LayoutInflater inflater;
    private Context context;
    private final int TRANSPARENT_BLACK = 0x64000000;
    private final String URL_IMAGE = Config.getInstance().PUBLIC_URL_API + "coupon/";

}
