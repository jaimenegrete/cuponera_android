package com.admin.devalley.cuponera.fragments;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.admin.devalley.cuponera.R;
import com.admin.devalley.cuponera.config.Config;
import com.admin.devalley.cuponera.helpers.JSONToModelsConverter;
import com.admin.devalley.cuponera.interfaces.OnStablishmentsMapFragmentInteractionListener;
import com.admin.devalley.cuponera.models.Model_Stablishment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestHandle;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ForkJoinTask;

public class StablishmentsMapFragment extends Fragment implements LocationListener , AdapterView.OnItemSelectedListener{
    public static boolean isShown;

    public static StablishmentsMapFragment newInstance() {
        return new StablishmentsMapFragment();
    }

    public StablishmentsMapFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        allStablishmentsArray = new ArrayList<Model_Stablishment>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getActionBar().setTitle("Cerca de mi");
        mainStablishmentsMapView = inflater.inflate(R.layout.fragment_mapa_cupones, container, false);
        stablishmentsMapView = (MapView) mainStablishmentsMapView.findViewById(R.id.mapView);
        stablishmentsMapView.onCreate(savedInstanceState);
        Spinner stablishmentTypeSpinner = (Spinner) mainStablishmentsMapView.findViewById(R.id.stablishmentTypesSpinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.stablishmentsTypesArray, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        stablishmentTypeSpinner.setAdapter(adapter);
        stablishmentTypeSpinner.setOnItemSelectedListener(this);
        stablishmentTypeSpinner.bringToFront();

        AsyncHttpClient asyncClient = new AsyncHttpClient();

        String url = Config.MAIN_URL_API + STABLISHMENTS_URL_API;
        RequestHandle requestHandle = asyncClient.get(url, null, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode,
                                  Header[] headers,
                                  Throwable throwable,
                                  JSONObject errorResponse) {
                Toast.makeText(getActivity(), "Ocurrió un error en el servidor", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray jsonStablishmentsArray) {
                allStablishmentsArray.clear();
                for (int i = 0; i < jsonStablishmentsArray.length(); i++) {
                    try {
                        JSONObject stablishmentJSONObject = jsonStablishmentsArray.getJSONObject(i);
                        Model_Stablishment stablishment = JSONToModelsConverter.convertJSONToStablishment(stablishmentJSONObject);
                        allStablishmentsArray.add(stablishment);
                    } catch (JSONException JSONException) {
                        JSONException.printStackTrace();
                        Toast.makeText(getActivity(),"Establecimiento inválido :(",Toast.LENGTH_SHORT).show();
                    }
                }
                setUpMapIfNeeded(allStablishmentsArray);
            }
        });

        // GPS STUFF
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);


        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            isGPSEnabled = false;
        } else {
            isGPSEnabled = true;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                30000, 0, this);
        return mainStablishmentsMapView;
    }

    @Override
    public void onLocationChanged(Location location) {

        if(stablishmentsMap != null){
            locationManager.removeUpdates(this);
            stablishmentsMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 10));
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            stablishmentsMapListener = (OnStablishmentsMapFragmentInteractionListener) activity;
        } catch (ClassCastException exception) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        stablishmentsMapListener = null;
        StablishmentsMapFragment.isShown = false;
        locationManager.removeUpdates(this);
    }

    @Override
    public void onResume(){
        super.onResume();
        stablishmentsMapView.onResume();
    }

    @Override
    public void onPause(){
        stablishmentsMapView.onPause();
        super.onPause();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String stablishmentType = parent.getItemAtPosition(position).toString();
        filterStablishmentsByType(stablishmentType);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void setUpMapIfNeeded(final List<Model_Stablishment> stablishmentsList) {
        // Needs to call MapsInitializer before doing any CameraUpdateFactory calls
        try {
            stablishmentsMap = stablishmentsMapView.getMap();
            stablishmentsMap.clear();
            stablishmentsMap.getUiSettings().setMyLocationButtonEnabled(true);
            stablishmentsMap.setMyLocationEnabled(true);

            MapsInitializer.initialize(this.getActivity());

            //stablishmentsMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude()), 10));
            stablishmentsMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {
                    for (Model_Stablishment stablishment : stablishmentsList) {
                        if (stablishment.name.equals(marker.getTitle())) {
                            stablishmentsMapListener.onMarkerSelected(stablishment);
                        }
                    }

                }

            });

            for (Model_Stablishment stablishment: stablishmentsList) {
                LatLng tmpLocation = new LatLng(stablishment.latitude, stablishment.longitude);
                Marker marker = stablishmentsMap.addMarker(new MarkerOptions()
                        .position(tmpLocation)
                        .title(stablishment.name)
                        .snippet(stablishment.address)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_launcher)));
            }

            stablishmentsMap.setMyLocationEnabled(true);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    private ActionBar getActionBar() {
        return ((ActionBarActivity) getActivity()).getSupportActionBar();
    }

    private void filterStablishmentsByType(String stablishmentType){
        List<Model_Stablishment> filteredStablishmentsArray = new ArrayList<>();
        for(Model_Stablishment stablishment: allStablishmentsArray){
            if(stablishment.stablishmentType.equals(stablishmentType)){
                filteredStablishmentsArray.add(stablishment);
            }
        }

        setUpMapIfNeeded(filteredStablishmentsArray);
    }

    private OnStablishmentsMapFragmentInteractionListener stablishmentsMapListener;
    private View mainStablishmentsMapView;
    private GoogleMap stablishmentsMap;
    private LatLng userLocation;
    private Location lastKnownLocation;
    private LocationManager locationManager;
    private Boolean isGPSEnabled;
    private List<Model_Stablishment> allStablishmentsArray;
    private MapView stablishmentsMapView;
    private final String STABLISHMENTS_URL_API = "stablishments";
}
