package com.admin.devalley.cuponera.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.admin.devalley.cuponera.R;
import com.admin.devalley.cuponera.config.Config;
import com.admin.devalley.cuponera.models.Model_Coupon;
import com.loopj.android.image.SmartImageView;

/**
 * Created by user on 07/04/2015.
 */
public class CouponStatsFragment extends Fragment{

    public Model_Coupon modelCoupon;

    public static CouponStatsFragment newInstance(Model_Coupon modelCoupon) {
        CouponStatsFragment fragment = new CouponStatsFragment();
        fragment.modelCoupon = modelCoupon;
        return fragment;
    }

    public CouponStatsFragment(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mainAdminCouponView =  inflater.inflate(R.layout.fragment_coupon_stats, container, false);
        ((SmartImageView)mainAdminCouponView.findViewById(R.id.couponImageView)).setImageUrl(COUPON_IMAGE_URL_ROUTE + modelCoupon.image);
        ((TextView)mainAdminCouponView.findViewById(R.id.couponDescription)).setText(modelCoupon.description);
        ((TextView)mainAdminCouponView.findViewById(R.id.couponAcquired)).setText(""+modelCoupon.acquired);
        ((TextView)mainAdminCouponView.findViewById(R.id.couponDownloads)).setText(""+modelCoupon.downloaded);
        ((TextView)mainAdminCouponView.findViewById(R.id.couponUsed)).setText(""+modelCoupon.used);

        return mainAdminCouponView;
    }

    private View mainAdminCouponView;
    private final String COUPON_IMAGE_URL_ROUTE = Config.getInstance().PUBLIC_URL_API + "coupon/";

}
