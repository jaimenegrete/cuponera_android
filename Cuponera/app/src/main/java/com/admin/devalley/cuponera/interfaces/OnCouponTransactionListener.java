package com.admin.devalley.cuponera.interfaces;

import com.admin.devalley.cuponera.models.Model_Coupon;

/**
 * Created by user on 07/04/2015.
 */
public interface OnCouponTransactionListener {
    public void onCouponAcquire(Model_Coupon modelCoupon);
    public void onCouponClientRedeem(Model_Coupon modelCoupon);
    public void onCouponAdminRedeem(Model_Coupon modelCoupon);
}
